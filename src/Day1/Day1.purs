module Day1 where

import Prelude (bind, map, pure, ($))
import Node.FS.Aff (readTextFile)
import Node.Encoding (Encoding(..))
import Data.Maybe (fromMaybe)
import Data.String (Pattern(..), split)
import Data.Int (fromString)
import Data.Traversable (sum)
import Effect.Aff

frequency :: Aff Int
frequency = do
  input <- readTextFile UTF8 "./data/day1"
  pure $ sum $ map (fromMaybe 0) $ map fromString $ split (Pattern "\n") input
