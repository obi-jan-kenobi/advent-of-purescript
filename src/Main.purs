module Main where

import Prelude (bind, show, ($), Unit)
import Effect (Effect)
import Effect.Console (log)
import Effect.Class (liftEffect)
import Day1 (frequency)
import Effect.Aff (Fiber, launchAff)

main :: Effect (Fiber Unit)
main = launchAff do
  freqs <- frequency
  liftEffect $ log $ show freqs
